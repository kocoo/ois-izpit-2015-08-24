var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});


app.get('/api/seznam', function(req, res) {
	res.send(noviceSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje novic)
 */
app.get('/api/dodaj', function(req, res) {
  var idd = noviceSpomin[(Object.keys(noviceSpomin).length)-1].id;
  idd++;
  var  novicatoAdd = {
    id: idd,
    naslov: req.query.naslov,
    povzetek: req.query.povzetek,
    kategorija:req.query.kategorija,
    postnaStevilka: parseInt(req.query.postnaStevilka),
    kraj: req.query.kraj,
    povezava: req.query.povezava
  };

  noviceSpomin.push(novicatoAdd);
  res.redirect('/');
  
});



/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje novice)
 */
app.get('/api/brisi', function(req, res) {
  // ...
  //res.send('Potrebno je implementirati brisanje novice!');
  // ...
  console.log("dobil: " + req.query);
  
  for(var i in noviceSpomin){
        var currNaslov = noviceSpomin[i].id;
        if(currNaslov == req.query.id) {
            console.log("BINGOOOOO");
            noviceSpomin.splice(i, 1);
            res.redirect('/');
            break;
        }
    }
    res.send("Novica z id-jem <id> ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
    //res.redirect('/');
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var noviceSpomin = [
  {
    id: 1,
    naslov: 'Slovenija in korupcija: končali smo v družbi Mehike in Kolumbije',
    povzetek: 'Slovenija krši mednarodne zaveze v boju proti podkupovanju, opozarjajo pri slovenski podružnici TI. Konvencijo o boju proti podkupovanju tujih javnih uslužbencev v mednarodnem poslovanju OECD namreč izvajamo "malo ali nič".',
    kategorija: 'novice',
    postnaStevilka: 1000,
    kraj: 'Ljubljana',
    povezava: 'http://www.24ur.com/novice/slovenija/slovenija-in-korupcija-koncali-smo-v-druzbi-mehike-in-kolumbije.html'
  }, {
    id: 2,
    naslov: 'V Postojni udaren začetek festivala z ognjenim srcem',
    povzetek: 'V Postojni se je z nastopom glasbenega kolektiva The Stroj začel tradicionalni dvotedenski festival Zmaj ma mlade.',
    kategorija: 'zabava',
    postnaStevilka: 6230,
    kraj: 'Postojna',
    povezava: 'http://www.rtvslo.si/zabava/druzabna-kronika/v-postojni-udaren-zacetek-festivala-z-ognjenim-srcem/372125'
  }
];
